#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/dir.h>

struct Directory {
	struct dirent **items;
	int num;
};

void
err(int code, char *msg)
{
	printf("Status: %d\n", code);
	printf("Content-Type: text/plain\n\n");
	printf("%s", msg);
	exit(code);
}

struct Directory
get_files(char *dirname) {
	struct Directory outp;
	outp.items = calloc(1, sizeof(struct dirent **));

	DIR *d;
	struct dirent *dir;
	d = opendir(dirname);

	if (!d) {
		err(500, "ERROR: Could not open directory.\n");
	}

	outp.num = 0;
	while ((dir = readdir(d)) != NULL) {
		outp.items = realloc(outp.items, (outp.num + 1) * sizeof(struct dirent **));
		outp.items[outp.num] = dir;
		outp.num++;
	}

	closedir(d);
	return outp;
}

int
main(void) {
	char *url = getenv("PATH_INFO");

	if (!url) {
		err(404, "ERROR: No path given.\n");
		return 1;
	}

	int len = strlen(url) + 2;
	char *path = malloc(len);
	snprintf(path, len, ".%s", url);
	struct Directory files = get_files(path);
	free(path);

	printf("Content-Type: text/html\n\n");
	printf("<!DOCTYPE html><html lang=\"en\"><head>");
	printf("</head><body>");
	printf("<p>%d files found.</p>\n", files.num);
	printf("<table>");
	for (int i = 0; i < files.num; i++) {
		printf("<tr>\n<td>");
		printf("<a href=\"%s\">%s</p>\n", files.items[i]->d_name,
					 files.items[i]->d_name);
		printf("</td>\n</tr>");
	}
	printf("</table>\n");
	printf("</body></html>");
	return 0;
}
