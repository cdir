CFLAGS = -std=c99 -Wall -Wextra -pedantic -g

all: man binary

binary:
	${CC} cdir.c -o cdir `pkg-config --libs libarchive` ${LDFLAGS} \
		-std=c99 ${CFLAGS}

man:
	scdoc < man/cdir.1.scd > man/cdir.1
	scdoc < man/cdirrc.5.scd > man/cdirrc.5

install:
	mkdir -p ${DESTDIR}${PREFIX}/bin ${DESTDIR}${PREFIX}/man
	cp cdir ${DESTDIR}${PREFIX}/bin/cdir
	cp cdir.1 ${DESTDIR}${PREFIX}/man/man1/cdir.1

uninstall:
	rm ${DESTDIR}${PREFIX}/bin/cdir
	rm ${DESTDIR}${PREFIX}/man/man1/cdir.1

devserver: binary
	caddy run --config ./contrib/Caddyfile
